package com.example.artspace

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.artspace.ui.theme.ArtSpaceTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ArtSpaceTheme {
                ArtAppLayout()
            }
        }
    }
}

@Composable
fun ArtAppLayout() {
    var currentStep by remember { mutableIntStateOf(1) }

    Surface(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
        color = Color.White
    ) {
        when (currentStep) {
            1 -> {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(10.dp)
                ) {
                    ArtPanel(
                        resourceDrawable = R.drawable.image1,
                        contentDescription = R.string.art_content_description
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ArtDescription(
                        title = R.string.title1,
                        artist = R.string.artist1,
                        year = R.string.year1
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ButtonPanel(
                        onBackward = { currentStep = 5 },
                        onForward = { currentStep = 2 }
                    )
                }
            }

            2 -> {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(10.dp)
                ) {
                    ArtPanel(
                        resourceDrawable = R.drawable.image2,
                        contentDescription = R.string.art_content_description
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ArtDescription(
                        title = R.string.title2,
                        artist = R.string.artist2,
                        year = R.string.year2
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ButtonPanel(
                        onBackward = { currentStep = 1 },
                        onForward = { currentStep = 3 })
                }
            }

            3 -> {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(10.dp)
                ) {
                    ArtPanel(
                        resourceDrawable = R.drawable.image3,
                        contentDescription = R.string.art_content_description
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ArtDescription(
                        title = R.string.title3,
                        artist = R.string.artist3,
                        year = R.string.year3
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ButtonPanel(
                        onBackward = { currentStep = 2 },
                        onForward = { currentStep = 4 })
                }
            }

            4 -> {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(10.dp)
                ) {
                    ArtPanel(
                        resourceDrawable = R.drawable.image4,
                        contentDescription = R.string.art_content_description
                    )
                    Spacer(modifier = Modifier.height(10.dp))
                    ArtDescription(
                        title = R.string.title4,
                        artist = R.string.artist4,
                        year = R.string.year4
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ButtonPanel(
                        onBackward = { currentStep = 3 },
                        onForward = { currentStep = 5 })
                }
            }

            5 -> {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(20.dp)
                ) {
                    ArtPanel(
                        resourceDrawable = R.drawable.image5,
                        contentDescription = R.string.art_content_description
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ArtDescription(
                        title = R.string.title5,
                        artist = R.string.artist5,
                        year = R.string.year5
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ButtonPanel(
                        onBackward = { currentStep = 4 },
                        onForward = { currentStep = 1 })
                }
            }

        }
    }
}


@Composable
fun ArtPanel(
    resourceDrawable: Int,
    contentDescription: Int
) {
    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .height(500.dp),
        color = Color.LightGray
    ) {
        Image(
            painter = painterResource(
                id = resourceDrawable
            ),
            contentDescription = stringResource(id = contentDescription),
            contentScale = ContentScale.FillBounds,
            modifier = Modifier
                .padding(20.dp)
        )
    }
}

@Composable
fun ArtDescription(
    title: Int,
    artist: Int,
    year: Int
) {
    Surface(
        color = Color.LightGray
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp)
        ) {
            Text(
                text = stringResource(title),
                fontSize = 20.sp,
            )
            Column {
                Text(
                    text = stringResource(artist),
                    fontSize = 40.sp,
                )
                Text(text = stringResource(year), fontSize = 20.sp)
            }
        }
    }
}

@Composable
fun ButtonPanel(
    onBackward: () -> Unit,
    onForward: () -> Unit
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceAround,
        verticalAlignment = Alignment.Bottom
    ) {
        Button(
            onClick = onBackward,
            colors = ButtonDefaults.buttonColors(Color.LightGray),
        ) {
            Text(
                text = stringResource(id = R.string.previous),
                textAlign = TextAlign.Center,
                modifier = Modifier.width(100.dp),
                fontSize = 20.sp,
                color = Color.Black
            )
        }
        Button(
            onClick = onForward,
            colors = ButtonDefaults.buttonColors(Color.DarkGray)

        ) {
            Text(
                text = stringResource(id = R.string.next),
                textAlign = TextAlign.Center,
                modifier = Modifier.width(100.dp),
                fontSize = 20.sp,
                color = Color.White
            )
        }
    }
}

@Preview
@Composable
fun ArtAppPreview() {
    ArtSpaceTheme {
        ArtAppLayout()
    }
}